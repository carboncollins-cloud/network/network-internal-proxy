# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2023-02-18]

### Changed
- Updated configuration for new Datacenter "Systems of Chaos"
- Update to Traefik v3.0.0-beta2
- Update job file name from `job.template.nomad` to `job.template.nomad.hcl` as per new reccomendations

## [2022-05-08]

### Changed
- constraint to use hostname instead of meta for now

## [2022-05-01]

### Removed
- Axion static routing
- Axion Syncthing static routing

## [2022-04-22]

### Added
- custom name for Nomad job

### Changed
- Updated all links to point to new repository location
- Moved repository to [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) GitLab Group
