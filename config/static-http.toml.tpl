[http]
  [http.middlewares]
    [http.middlewares.routerHost.headers]
      [http.middlewares.routerHost.headers.customRequestHeaders]
        x-c3-host = "{{ env "node.unique.name" }}"
      [http.middlewares.routerHost.headers.customResponseHeaders]
        x-c3-host = "{{ env "node.unique.name" }}"

    [http.middlewares.securityHeaders.headers]
      frameDeny = true
      contentTypeNosniff = true
      browserXssFilter = true

      [http.middlewares.securityHeaders.headers.customResponseHeaders]
        server = ""
        x-envoy-upstream-service-time = ""
        x-powered-by = ""
        x-application-version = ""
        x-download-options = "noopen"

    [http.middlewares.errorPages.errors]
      status = ["503"]
      service = "errorPages"
      query = "/{status}.html"


  [http.routers]
    [http.routers.nomad]
      entryPoints = ["https"]
      rule = "Host(`nomad.soc.carboncollins.se`)"
      service = "nomad"
      [http.routers.nomad.tls]
        certResolver = "lets-encrypt"
        [[http.routers.nomad.tls.domains]]
          main = "*.soc.carboncollins.se"

    [http.routers.consul]
      entryPoints = ["https"]
      rule = "Host(`consul.soc.carboncollins.se`)"
      service = "consul"
      [http.routers.consul.tls]
        certResolver = "lets-encrypt"
        [[http.routers.consul.tls.domains]]
          main = "*.soc.carboncollins.se"

    [http.routers.vault]
      entryPoints = ["https"]
      rule = "Host(`vault.soc.carboncollins.se`)"
      service = "vault"
      [http.routers.vault.tls]
        certResolver = "lets-encrypt"
        [[http.routers.vault.tls.domains]]
          main = "*.soc.carboncollins.se"

    [http.routers.unifi]
      entryPoints = ["https"]
      rule = "Host(`unifi.soc.carboncollins.se`)"
      service = "unifi"
      [http.routers.unifi.tls]
        certResolver = "lets-encrypt"
        [[http.routers.unifi.tls.domains]]
          main = "*.soc.carboncollins.se"

  [http.services]
    # Error Page Side Car 
    [http.services.errorPages.loadBalancer]
      [http.services.errorPages.loadBalancer.healthCheck]
        path = "/health"
        interval = "10s"
        timeout = "3s"

      [http.services.errorPages.loadBalancer.sticky.cookie]
      [[http.services.errorPages.loadBalancer.servers]]
        url = "http://127.0.0.1:{{ env "NOMAD_PORT_errors" }}"


    # Nomad 
    [http.services.nomad.loadBalancer]
      [http.services.nomad.loadBalancer.healthCheck]
        path = "/v1/agent/health"
        interval = "10s"
        timeout = "3s"

      [http.services.nomad.loadBalancer.sticky.cookie]
      [[http.services.nomad.loadBalancer.servers]]
        url = "http://10.0.60.100:4646/"
      [[http.services.nomad.loadBalancer.servers]]
        url = "http://10.0.60.101:4646/"
      [[http.services.nomad.loadBalancer.servers]]
        url = "http://10.0.60.102:4646/"

    # Consul
    [http.services.consul.loadBalancer]
      # this should have ca defined
      serversTransport = "insecureSkipVerify"

      [http.services.consul.loadBalancer.healthCheck]
        path = "/ui"

      [http.services.consul.loadBalancer.sticky.cookie]
      [[http.services.consul.loadBalancer.servers]]
        url = "https://10.0.60.100:8501/"
      [[http.services.consul.loadBalancer.servers]]
        url = "https://10.0.60.101:8501/"
      [[http.services.consul.loadBalancer.servers]]
        url = "https://10.0.60.102:8501/"

    # Vault
    [http.services.vault.loadBalancer]
      # this should have ca defined
      serversTransport = "insecureSkipVerify"

      [http.services.vault.loadBalancer.healthCheck]
        path = "/ui"

      [http.services.vault.loadBalancer.sticky.cookie]
      [[http.services.vault.loadBalancer.servers]]
        url = "https://10.0.60.100:8200/"
      [[http.services.vault.loadBalancer.servers]]
        url = "https://10.0.60.101:8200/"
      [[http.services.vault.loadBalancer.servers]]
        url = "https://10.0.60.102:8200/"

    # UniFi
    [http.services.unifi.loadBalancer]
      serversTransport = "insecureSkipVerify"

      [http.services.unifi.loadBalancer.healthCheck]
        path = "/api/system"

      [[http.services.unifi.loadBalancer.servers]]
        url = "https://10.0.0.10:443/"

  # Transports
  [http.serversTransports]
    [http.serversTransports.insecureSkipVerify]
      insecureSkipVerify = true

[tls.options]
  [tls.options.tlsdns]
    minVersion = "VersionTLS13"
    sniStrict = false # Want to see if routing games works now
    alpnProtocols = ["dot"]
