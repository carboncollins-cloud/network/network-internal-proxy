
[log]
  level = "DEBUG"

[providers.consulCatalog]
  serviceName = "internal-proxy"
  prefix = "internal-proxy"
  exposedByDefault = false
  connectAware = true
  connectByDefault = true
  defaultRule = "Host(`{ { `{ { normalize .Name }}` }}.soc.carboncollins.se`)"

[providers.file]
  filename = "/etc/traefik/static-http.toml"

[entryPoints]
  [entryPoints.http]
    address = ":{{ env "NOMAD_PORT_http" }}"
    [entryPoints.http.http]
      middlewares = ["routerHost@file", "errorPages@file"]

      [entryPoints.http.http.redirections]
        [entryPoints.http.http.redirections.entryPoint]
          to = "https"
          scheme = "https"

  [entryPoints.https]
    address = ":{{ env "NOMAD_PORT_https" }}"
    [entryPoints.https.forwardedHeaders]
      trustedIPs = ["{{ env "attr.unique.network.ip-address" }}"]
    [entryPoints.https.http]
      middlewares = ["routerHost@file", "securityHeaders@file", "errorPages@file"]

  [entryPoints.traefik]
    address = ":{{ env "NOMAD_PORT_dashboard" }}"
    [entryPoints.traefik.http]
      middlewares = ["routerHost@file"]

  [entryPoints.tcpdns]
    address = ":{{ env "NOMAD_PORT_dns" }}"

  [entryPoints.udpdns]
    address = ":{{ env "NOMAD_PORT_dns" }}/udp"

  [entryPoints.tlsdns]
    address = ":{{ env "NOMAD_PORT_tlsdns" }}"

  [entryPoints.mysql]
    address = ":{{ env "NOMAD_PORT_mysql" }}"

  [entryPoints.postgresql]
    address = ":{{ env "NOMAD_PORT_postgresql" }}"

  [entryPoints.metrics]
    address = ":8082"

[api]
  dashboard = true
  insecure  = true

[serversTransport]
  insecureSkipVerify = true

[tls.options]
  [tls.options.default]
    minVersion = "VersionTLS13"
    sniStrict = true
    preferServerCipherSuites = true

[certificatesResolvers.lets-encrypt.acme]
  {{ with secret "c3kv/data/api/cloudflair/carboncollins.se" }}
  email = "{{ index .Data.data "email" }}"
  {{ end }}
  storage = "/acme/lets-encrypt.json"

  [certificatesResolvers.lets-encrypt.acme.dnsChallenge]
    provider = "cloudflare"
    resolvers = ["1.1.1.1:53", "1.0.0.1:53"]

[pilot]
  dashboard = false

# [tracing]
#  serviceName = "internal-proxy"

#   [tracing.zipkin]
#     httpEndpoint = "http://tempo-zipkin.connect.cloud.carboncollins.se/api/v2/spans"

[metrics]
  [metrics.prometheus]
    entryPoint = "metrics"
