job "network-internal-proxy" {
  name        = "Network Internal Proxy (Traefik)"
  type        = "service"
  region      = "se"
  datacenters = ["soc"]
  namespace   = "c3-networking"

  priority = 90

  constraint {
    attribute = "${meta.c3.keepalived.ioc-ipv4.name}"
    operator  = "is_set"
  }

  constraint {
    distinct_hosts = true
  }

  group "soc" {
    count = 2

    consul {}

    volume "traefik-data" {
      type            = "csi"
      source          = "traefik-data"
      attachment_mode = "file-system"
      access_mode     = "single-node-writer"
      per_alloc       = true
    }

    network {
      mode = "bridge"

      dns {
        servers = [
          "1.1.1.1",
          "1.0.0.1"
        ]
      }

      port "http" {
        static = "80"
        to     = "80"
      }

      port "https" {
        static = "443"
        to     = "443"
      }

      port "dns" {
        static = "53"
        to     = "53"
      }

      port "tlsdns" {
        static = "853"
        to     = "853"
      }

      port "mysql" {
        static = "3306"
        to     = "3306"
      }

      port "postgresql" {
        static = "5432"
        to     = "5432"
      }

      port "dashboard" {
        static = "8080"
        to     = "8080"
      }

      port "errors" {}
    }

    service {
      provider = "consul"
      name     = "internal-proxy"
      port     = "${NOMAD_PORT_dashboard}"
      task     = "traefik"

      connect {
        native = true
      }

      check {
        name     = "alive"
        type     = "tcp"
        port     = "http"
        interval = "10s"
        timeout  = "2s"
      }
    }

    task "traefik" {
      driver = "docker"

      volume_mount {
        volume      = "traefik-data"
        destination = "/acme"
        read_only   = false
      }

      vault {
        role = "service-cloudflair-carboncollins-se"
      }

      resources {
        cpu    = 500
        memory = 300
      }

      config {
        image = "[[ .traefikImage ]]"

        ports = [
          "http",
          "https",
          "dashboard",
          "dns",
          "mysql",
          "tlsdns",
          "postgresql"
        ]

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
          "local/static-http.toml:/etc/traefik/static-http.toml"
        ]
      }

      template {
        data = <<EOH
{{ with secret "c3kv/data/api/cloudflair/carboncollins.se" }}
CF_DNS_API_TOKEN="{{ index .Data.data "token" }}"
{{ end }}
        EOH

        destination = "secrets/cloudflare.env"
        env         = true
      }

      template {
        data = <<EOH
[[ fileContents "./config/static-http.toml.tpl" ]]
        EOH

        destination = "local/static-http.toml"
        change_mode = "noop"
      }

      template {
        data = <<EOH
[[ fileContents "./config/traefik.toml.tpl" ]]
        EOH

        destination = "local/traefik.toml"
      }
    }

    task "static-error-pages" {
      driver = "docker"

      lifecycle {
        hook = "poststart"
        sidecar = true
      }

      config {
        image = "[[ .swsImage ]]"

        ports = [
          "errors"
        ]

        volumes = [
          "local/public:/public"
        ]
      }

      env {
        SERVER_HOST = "0.0.0.0"
        SERVER_PORT = "${NOMAD_PORT_errors}"
        SERVER_ROOT = "/public"

        SERVER_HEALTH           = "true"
        SERVER_MAINTENANCE_MODE = "false"
      }

      template {
        data = <<EOH
[[ fileContents "./public/503.html" ]]
        EOH

        destination = "local/public/503.html"
      }
    }
  }

  reschedule {
    delay          = "10s"
    delay_function = "exponential"
    max_delay      = "10m"
    unlimited      = true
  }

  update {
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "10m"
    progress_deadline = "15m"
    auto_revert       = true
  }

  meta {
    gitSha      = "[[ .gitSha ]]"
    gitBranch   = "[[ .gitBranch ]]"
    pipelineId  = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId   = "[[ .projectId ]]"
    projectUrl  = "[[ .projectUrl ]]"
    statefull   = "true"
  }
}
