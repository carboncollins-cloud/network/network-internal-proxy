terraform {
  backend "consul" {
    path = "terraform/network-internal-proxy"
  }

  required_providers {
    nomad = {
      source = "hashicorp/nomad"
      version = "2.1.0"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.14.0"
    }
  }
}

provider "nomad" {}
provider "vault" {}

module "soc_volumes" {
  source = "../../modules/traefikNomadVolumes"

  plugin_id = "soc-axion-smb"

  cifs_user_id = 3205
  cifs_group_id = 3205
}
