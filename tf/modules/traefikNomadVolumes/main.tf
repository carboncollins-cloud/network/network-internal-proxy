terraform {
  required_providers {
    nomad = {
      source = "hashicorp/nomad"
      version = "2.1.0"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.14.0"
    }
  }
}

data "vault_kv_secret_v2" "volume_credentials" {
  mount = "c3kv"
  name  = "datacenter/soc/storage/axion/csi"
}

data "nomad_plugin" "storage" {
  plugin_id        = var.plugin_id
  wait_for_healthy = true
}

resource "nomad_csi_volume" "traefik_data" {
  count      = 2
  depends_on = [data.nomad_plugin.storage]

  lifecycle {
    prevent_destroy = true
  }

  namespace = "c3-networking"

  plugin_id    = data.nomad_plugin.storage.plugin_id
  volume_id    = format("traefik-data[%d]", count.index)
  name         = format("traefik-data[%d]", count.index)
  capacity_min = "1GiB"
  capacity_max = "2GiB"

  capability {
    access_mode     = "single-node-writer"
    attachment_mode = "file-system"
  }

  mount_options {
    fs_type = "cifs"
    mount_flags = [
      "vers=3.0",
      format("uid=%d", var.cifs_user_id),
      format("gid=%d", var.cifs_group_id),
      "file_mode=0600",
      "dir_mode=0700",
      "noperm",
      "nobrl",
      format("username=%s", data.vault_kv_secret_v2.volume_credentials.data["user"]),
      format("password=%s", data.vault_kv_secret_v2.volume_credentials.data["pass"])
    ]
  }
}
